#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# mi_rss.py
#
# Librería para importar rss
#
# Acortar URLs: https://www.indetectables.net/viewtopic.php?t=54685
#
# Librería newspaper: https://newspaper.readthedocs.io/en/latest/
# Ejemplos de uso de newspaper: https://www.programcreek.com/python/example/88066/newspaper.Article

# CONSTANTES
ACORTADOR = 'http://tinyurl.com/api-create.php?url='
ALMACENADOS = 5 # Número de URL de noticias de cada medio que se almacenan para no repetir
FRECUENCIA = 60 # Frecuencia por defecto para actualizar (antigüedad noticias que se leen)
MASTODON_MAX = 500 # Número de caracteres máximo (para acortar la presentación de la noticia)
MAX_NOTICIAS = 3 # Noticias a leer de cada medio por defecto en cada iteración
# Dimensiones fotos
MI_ANCHO = 480
MI_ALTO = 320
MIN_ANCHO = 100
MIN_ALTO = 100

# LIBRERÍAS
from dateparser import parse as parse_date
from datetime import datetime, timedelta
from dateutil import tz
from io import BytesIO
from magic import Magic
from mi_noticias import Datos_origenes, Ult_notic_proc
from newspaper import Article
from PIL import Image
from sys import exc_info as lee_error
import re
import requests

# FUNCIONES
def _a_fecha_local( fecha ):
    # Transforma una fecha UTC en fecha española
    zona = tz.gettz( 'Europe/Madrid' )
    fecha_local = fecha.astimezone( zona )
    return fecha_local

def _acortar_url( url_orig ):
    # Acorta una URL
    shortUrl = requests.get( ACORTADOR + url_orig )
    mi_url =  shortUrl.content.decode( 'utf-8' )
    return mi_url

def _descargar_noticia( url ):
    # Descargar una noticia dada su url
    try:
        mi_noticia = Article( url, language='es' )
        mi_noticia.download()
        mi_noticia.parse()
    except:
        mi_noticia = None
        print( 'Leer_noticias() => ERROR al leer noticia:', sys.exc_info()[0] )
    return mi_noticia

def _html2texto( txt_orig ):
    # Pasa una noticia en html a texto
    texto = txt_orig
    texto = re.sub( r'<[^)]*>', '', texto )
    texto = re.sub( r'<[^)]*', '', texto )
    # En algunos medios se queda parte de código y siempre terminar en '>'
    patron = re.compile( r'.*>', re.DOTALL )
    texto = patron.sub( '', texto ).strip()
    return texto

def _leer_noticias( dbx, nom_orig, nom_ult, path_fich, fecha_limite, max_notic = 5, verbose = False ):
    # Lee las noticias del listado de rss y devuelve un listado de noticias
    dorig = Datos_origenes( dbx, nom_orig, path_fich, verbose )
    lista_origenes = dorig.leer_origenes()

    lista_noticias = []
    unp = Ult_notic_proc( dbx, nom_ult, path_fich, ALMACENADOS, verbose )

    for origen in lista_origenes:
        medio = origen['nombre']

        rss = dorig.leer_rss( origen )
        if rss != None:
            contador = 0
            for noticia in rss.entries:
                if 'published' in noticia:
                    fecha_local = _a_fecha_local( parse_date( noticia['published'] ) )
                else:
                    fecha_local = fecha_limite
                    if verbose: print( 'Leer_noticias() => ERROR: Noticia sin fecha de publicación' )

                if 'link' in noticia:
                    url_orig = noticia['link']
                else:
                    url_orig = ''
                    if verbose: print( 'Leer_noticias() => ERROR: Noticia sin link' )

                if 'summary' in noticia:
                    summary = noticia['summary']
                else:
                    summary = ''
                    if verbose: print( 'Leer_noticias() => ERROR: Noticia sin resumen' )

                if url_orig != '':
                    if not( unp.existe_dato( medio, url_orig ) ) and ( fecha_local >= fecha_limite ):
                        mi_noticia = _descargar_noticia( url_orig )

                        if mi_noticia != None:
                            mi_url = _acortar_url( url_orig )

                            titular = mi_noticia.title.strip()

                            if len( mi_noticia.summary ) > 0:
                                texto = mi_noticia.summary
                            elif len( summary ) >0:
                                # El campo .summary falla la mayor parte de las veces
                                texto = _html2texto( summary )
                            else:
                                texto = mi_noticia.text

                            primera = True
                            resumen = ''
                            for c in texto:
                                if primera:
                                    primera = False
                                    anterior = c
                                    resumen = c
                                else:
                                    if not( ord( c ) == 10 and ord( anterior ) == 10 ):
                                        resumen = resumen + c
                                    anterior = c
                            resumen = resumen.strip()

                            media = []
                            img_local = requests.get( mi_noticia.top_image ).content
                            mi_magic = Magic( mime = True )
                            tipo_mime = mi_magic.from_buffer( img_local )
                            media.append( { 'url':mi_noticia.top_image, 'tipo':tipo_mime } )

                            lista_noticias.append( { 'medio':medio, 'url':mi_url, 'fecha':fecha_local,
                                                     'titular':titular, 'resumen':resumen, 'media':media,
                                                     'url_orig':url_orig } )
                            unp.escribir_dato( medio, url_orig )
                            if verbose: print( 'Leer_noticias() => ' + url_orig + ' añadida' )
                        else:
                            if verbose: print( 'Leer_noticias() => La URL ' + url_orig + ' no contiene ninguna noticia' )
                    else:
                        if verbose: print( 'Leer_noticias() => Noticia ' + url_orig + ' ya publicada' )

                contador += 1
                if contador >= max_notic:
                    break
        if verbose: print( 'Leer_noticias() => Leídas ' +str( contador ) + ' noticias de ', origen['nombre'] )

    unp.cargar_dbx()
    return lista_noticias

def _noticia2toot( noticia ):
    salida = 'Medio: ' + noticia['medio']
    salida = salida + '\n\n' + noticia['titular']
    if noticia['resumen'] != '':
        salida = salida + '\n\n' + noticia['resumen']

    cola = '\n\n' + noticia['url']

    max_long = MASTODON_MAX - len( cola )
    if len( salida ) > max_long:
        cierre = '[...]'
        salida = salida[ 0 : max_long - len( cierre ) ]
        salida = salida + cierre

    salida = salida + cola
    return salida

def _es_multimedia( tipo ):
    if ( tipo.find( 'audio' ) >= 0 ) or ( tipo.find( 'video' ) >= 0 ) or ( tipo.find( 'image' ) >= 0 ):
        return True
    else:
        return False

def _subir_fotos( masto, fotos ):
    salida = []
    for foto in fotos:
        if _es_multimedia( foto['tipo'] ):
            if foto['tipo'].find( 'image' ) == 0:
                try:
                    img_local = requests.get( foto['url'] ).content
                    mi_foto = Image.open( BytesIO( img_local ) )

                    ancho, alto = mi_foto.size
                    if ancho > MI_ANCHO:
                        alto = round( alto * ( MI_ANCHO / ancho ) )
                        ancho = MI_ANCHO
                    if alto > MI_ALTO:
                        ancho = round( ancho * ( MI_ALTO / alto ) )
                        alto = MI_ALTO
                    mi_foto = mi_foto.resize( ( ancho, alto ), Image.ANTIALIAS )

                    buf = BytesIO()
                    mi_foto.save(buf, format='PNG' )

                    img_local2 = buf.getvalue()

                    # Si la foto es muy pequeña no presentarla
                    if ancho > MIN_ANCHO and alto > MIN_ALTO:
                        nueva_foto = masto.mastodon.media_post( img_local2, mime_type = 'image/png' )
                        salida.append( nueva_foto )
                except:
                    print( 'publicar_rss() => Medio no válido: ', foto['url'], '   TIPO = ', foto['tipo'] )
                    print( 'ERROR ', lee_error()[0] )
            else:
                try:
                    img_local = requests.get( foto['url'] ).content
                    nueva_foto = masto.mastodon.media_post( img_local, mime_type = foto['tipo'] )
                    # nueva_foto = ''
                    salida.append( nueva_foto )
                except:
                    print( 'publicar_rss() => Medio no válido: ', foto['url'], '   TIPO = ', foto['tipo'] )
                    print( 'ERROR ', lee_error()[0] )

    return salida

def Publicar_rss( dbx, masto, nom_fnotic, nom_fult, path_fich, frecuencia = FRECUENCIA, verbose = False ):
    # Publica en mastodon una serie de rss leídos del fichero nom_fnotic
    hora_limite = _a_fecha_local( datetime.now() ) - timedelta( minutes = frecuencia )
    noticias = _leer_noticias( dbx, nom_fnotic, nom_fult, path_fich, hora_limite, MAX_NOTICIAS, verbose )

    if verbose: print( 'publicar_rss() => Tratar noticias' )
    if noticias != None:
        if verbose: print( '---\n' )
        for noticia in noticias:
            cuerpo = _noticia2toot( noticia )
    
            fotos = _subir_fotos( masto, noticia['media'] )
            masto.mastodon.status_post( cuerpo, media_ids = fotos )
            if verbose: 
                print( cuerpo )
                print( 'FOTOS = ', len( fotos ) )
                print( '---\n' )

        total = len( noticias )
    else:
        total = 0

    print( 'TOTAL NOTICIAS =', total )
