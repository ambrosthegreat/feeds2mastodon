#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Lector de feeds RSS
#
# Acortar URLs: https://www.indetectables.net/viewtopic.php?t=54685
#
# Librería newspaper: https://newspaper.readthedocs.io/en/latest/
# Ejemplos de uso de newspaper: https://www.programcreek.com/python/example/88066/newspaper.Article

# CONSTANTES
INST_URL = 'https://botsin.space/' # Instancia del bot
# Datos de los rss
ORIGEN = 'origenes.txt'
ULTIMAS = 'ultimo_publicado.nfo'
PATH_LOCAL = 'ficheros_frases/'

# LIBRERÍAS
from argparse import ArgumentParser
from mi_dropbox import Mi_dropbox
from mi_masto import Mi_masto
from mi_rss import Publicar_rss

# FUNCIONES
def _args():
    parser = ArgumentParser( description = 'Publica noticias de feeds RSS' )
    parser.add_argument( '-d', '--dbx_token', help = 'Token de dropbox', type = str, required = True )
    parser.add_argument( '-i', '--inst_url', help = 'URL de la instancia del bot', type = str, required = False )
    parser.add_argument( '-t', '--app_token', help = 'Token de la app', type = str, required = True )
    parser.add_argument( '-v', '--verbose', help = 'Mostrar más mensajes', action = 'store_true', default = False )
    return parser.parse_args()

# MAIN
args = _args()
if args.inst_url:
    instancia = args.inst_url
else:
    instancia = INST_URL
masto = Mi_masto( instancia, args.app_token )
dbx = Mi_dropbox( args.dbx_token )
Publicar_rss( dbx, masto, ORIGEN, ULTIMAS, PATH_LOCAL, frecuencia = 12, verbose = args.verbose )
