#!/usr/bin/env python3
#
# mi_noiticias
#
# Librería para tratar los orígenes de rss y las últimas noticias procesadas

# CONSTANTES
RAIZ = 'url'

# LIBRERÍAS
from configparser import ConfigParser
from feedparser import parse as parse_rss
from mi_ficheros import Mi_ficheros
from sys import exc_info as lee_error

# FUNCIONES
class Datos_origenes():
    # Procesar orígenes de datos
    def __init__( self, dbx, nom_fich, path_fich, verbose = False ):
        # Parámetros: Objeto mi_dropbox y nombre y trayectoria para el fichero de orígenes
        self.dbx = dbx
        self.f_rss = Mi_ficheros( nom_fich, path_fich )
        self.verbose = verbose

    def leer_origenes( self, verbose = False ):
        # Lee los origenes de RSS del fichero nom_fnotic y los devuelve en una lista
        # path_fich es el directorio local en que están estos ficheros
        if self.dbx.descargar( self.f_rss.en_path(), self.f_rss.en_raiz() ):
            lineas = self.f_rss.lee_lineas()
        else:
            lineas = []

        lista_rss = []
        for linea in lineas:
            if len( linea.strip() ) >0:
                if linea.strip()[0] != '#':
                    nombre =  linea[ 0 : linea.find( ';' ) ].strip()
                    url = linea[ linea.find( ';' )+1 : len( linea ) ].strip()
                    lista_rss.append( { 'nombre':nombre, 'url':url } )

        if self.verbose: print( 'leer_origenes() => Leídos ', len( lista_rss ), ' orígenes de datos' )
        return lista_rss

    def leer_rss( self, mi_rss ):
        # Leer datos de un rss
        if self. verbose: print( 'leer_rss() => Leyendo ', mi_rss['nombre'], ' --- ', mi_rss['url'] )
        rss_leida = False
        try:
            salida = parse_rss( mi_rss['url'] )
        except:
            salida = None
            print( 'Leer_noticias() => ERROR al leer rss:', lee_error()[0] )
        return salida

class Ult_notic_proc():
    # Procesar últimas noticias leídas
    def __init__( self, dbx, nom_fich, path_fich, n_datos, verbose = False ):
        # Parámetros: Objeto mi_dropbox y nombre y trayectoria para el fichero de últimas noticias
        self.verbose = verbose
        self.dbx = dbx
        self.f_ult = Mi_ficheros( nom_fich, path_fich )
        self.n_datos = n_datos
        self.descargar_dbx()

    def escribir_dato( self, seccion, dato ):
        # Escribe un nuevo dato en las últimas noticias
        fich = ConfigParser()
        fich.read( self.f_ult.en_path() )

        if seccion in fich:
            for n in range( self.n_datos - 1 ):
                ultimo = RAIZ + str( self.n_datos - n )
                penultimo = RAIZ + str( self.n_datos - n - 1 )
                fich[seccion][ultimo] = fich[seccion][penultimo]
            fich[seccion][RAIZ + '1'] = dato
        else:
            fich[seccion] = {}
            for n in range( self.n_datos ):
                nom_dato = RAIZ + str( n+1 )
                fich[seccion][nom_dato] = ''

        with open( self.f_ult.en_path(), 'w') as mi_archivo:
            fich.write( mi_archivo )

    def existe_dato( self, seccion, dato ):
        # True si existe el dato en el fichero de últimas noticias
        salida = False
        fich = ConfigParser()
        fich.read( self.f_ult.en_path() )
        
        if seccion in fich:
            for n_url, dato_guard in fich[seccion].items():
                if dato_guard == dato:
                    salida = True
                    break

        return salida

    def cargar_dbx( self ):
        # Cargar fichero de últimas noticias a dropbox
        self.dbx.cargar( self.f_ult.en_path(), self.f_ult.en_raiz() )
        if self.verbose: print( 'Datos_noticias.cargar_dbx() => Fichero de últimas noticias cargado' )

    def descargar_dbx( self ):
        # Descargar fichero de últimas noticias de dropbox
        if self.dbx.descargar( self.f_ult.en_path(), self.f_ult.en_raiz() ):
            if self.verbose: print( 'Datos_noticias.descargar_dbx() => Fichero de últimas noticias descargado' )
        else:
            print( 'Datos_noticias.descargar_dbx() => No existe fichero de últimas noticias' )

